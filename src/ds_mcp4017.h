#ifndef DS_MCP4017_H
#define DS_MCP4017_H
/*
	���������� ��� �������� ���������� ���������� �� ���� mcp4017

	email: dik@Inbox.ru
*/

#include <Arduino.h>

class ds_mcp4017 {
	public:
		ds_mcp4017(uint8_t addr=0x2F,uint8_t maxSteps=128);

		bool setStep(uint8_t step);
		uint8_t getStep();

	private:
		uint8_t _addr;
		uint8_t _maxSteps;
};


#endif
