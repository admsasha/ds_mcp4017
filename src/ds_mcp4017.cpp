#include "ds_mcp4017.h"

#include <Wire.h>

ds_mcp4017::ds_mcp4017(uint8_t addr,uint8_t maxSteps) : _addr(addr),_maxSteps(maxSteps){
	Wire.begin();
}

bool ds_mcp4017::setStep(uint8_t step){
	if (step>=_maxSteps) return false;

	Wire.beginTransmission(_addr);
	Wire.write(step);
	Wire.endTransmission();

	return true;
}

uint8_t ds_mcp4017::getStep(){
	Wire.beginTransmission(_addr);
	Wire.endTransmission();
	Wire.requestFrom(_addr, (uint8_t)1);
	return Wire.read(); 
}
